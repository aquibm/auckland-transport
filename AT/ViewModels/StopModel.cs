﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace AT.ViewModels
{

    public class StopModel : INotifyPropertyChanged
    {
        private string _StopCode;
        public string StopCode
        {
            get
            {
                return _StopCode;
            }
            set
            {
                if (value != _StopCode)
                {
                    _StopCode = value;
                    NotifyPropertyChanged("StopCode");
                }
            }
        }

        private string _StopName;
        public string StopName
        {
            get
            {
                return _StopName;
            }
            set
            {
                if (value != _StopName)
                {
                    _StopName = value;
                    NotifyPropertyChanged("StopName");
                }
            }
        }

        private double _StopLat;
        public double StopLat
        {
            get
            {
                return _StopLat;
            }
            set
            {
                if (value != _StopLat)
                {
                    _StopLat = value;
                    NotifyPropertyChanged("StopLat");
                }
            }
        }

        private double _StopLon;
        public double StopLon
        {
            get
            {
                return _StopLon;
            }
            set
            {
                if (value != _StopLon)
                {
                    _StopLon = value;
                    NotifyPropertyChanged("StopLon");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}