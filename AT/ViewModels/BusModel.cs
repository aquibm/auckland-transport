﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace AT.ViewModels
{

    public class BusModel : INotifyPropertyChanged
    {
        private string _Route;
        public string Route
        {
            get
            {
                return _Route;
            }
            set
            {
                if (value != _Route)
                {
                    _Route = value;
                    NotifyPropertyChanged("Route");
                }
            }
        }

        private string _Destination;
        public string Destination
        {
            get
            {
                return _Destination;
            }
            set
            {
                if (value != _Destination)
                {
                    _Destination = value;
                    NotifyPropertyChanged("Destination");
                }
            }
        }

        private string _ScheduledTime;
        public string ScheduledTime
        {
            get
            {
                return _ScheduledTime;
            }
            set
            {
                if (value != _ScheduledTime)
                {
                    _ScheduledTime = value;
                    NotifyPropertyChanged("ScheduledTime");
                }
            }
        }

        private string _ExpectedTime;
        public string ExpectedTime
        {
            get
            {
                return _ExpectedTime;
            }
            set
            {
                if (value != _ExpectedTime)
                {
                    _ExpectedTime = value;
                    NotifyPropertyChanged("ExpectedTime");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}