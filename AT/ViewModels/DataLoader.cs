﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using AT.Resources;
using System.Windows;
using System.IO;
using System.Collections.Generic;

namespace AT.ViewModels
{
    public class DataLoader : INotifyPropertyChanged
    {

        public ObservableCollection<StopModel> Stops { get; private set; }

        public DataLoader()
        {
            this.Stops = new ObservableCollection<StopModel>();
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public void LoadStops()
        {

            try
            {
                var RS = Application.GetResourceStream(new Uri("Assets/Stops.txt", UriKind.Relative));
                if (RS != null)
                {
                    Stream FS = RS.Stream;
                    if (FS.CanRead)
                    {
                        StreamReader SR = new StreamReader(FS);
                        string[] StopsList = SR.ReadToEnd().Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                        foreach (string Stop in StopsList)
                        {
                            string[] StopData = Stop.Split(',');
                            this.Stops.Add(new StopModel() { StopCode = StopData[1], StopName = StopData[2], StopLat = Double.Parse(StopData[3]), StopLon = Double.Parse(StopData[4]) });
                        }
                    }
                    else throw new Exception("Boo");
                }
                else throw new Exception("Boo");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong.\nError: " + ex.Message);
            }

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}