﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AT.Resources;
using System.Collections.ObjectModel;
using AT.ViewModels;
using System.Threading.Tasks;

namespace AT
{
    public partial class MainPage : PhoneApplicationPage
    {

        private ObservableCollection<StopModel> RefinedSearch = new ObservableCollection<StopModel>();

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadStops();
            }
        }

        private async void Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            string Search = txtSearch.Text.ToLower();

            if (Search != null && Search.Length > 0)
            {
                ObservableCollection<StopModel> SearchStops = new ObservableCollection<StopModel>();

                await Task.Run(delegate
                {
                    var StopList = from s in App.ViewModel.Stops where s.StopCode.Contains(Search) || s.StopName.ToLower().Contains(Search) select s;

                    foreach (var Stop in StopList)
                    {
                        SearchStops.Add(Stop);
                    }
                });

                Deployment.Current.Dispatcher.BeginInvoke(() => StopView.ItemsSource = SearchStops);

            }
            else
            {
                StopView.ItemsSource = App.ViewModel.Stops;
            }
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            string Search = txtSearch.Text;

            if (Search != null && Search.Length > 0)
            {
                ObservableCollection<StopModel> SearchStops = new ObservableCollection<StopModel>();

                foreach (StopModel Stop in App.ViewModel.Stops)
                {
                    if (Stop.StopCode.Contains(Search) || Stop.StopName.Contains(Search))
                        SearchStops.Add(Stop);
                }

                StopView.ItemsSource = SearchStops;

            }
            else
            {
                StopView.ItemsSource = App.ViewModel.Stops;
            }
        }

        private void StopView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (StopView.SelectedItem == null)
                return;

            StopModel Stop = (StopView.SelectedItem as StopModel);
            PhoneApplicationService.Current.State["Stop"] = Stop;
            NavigationService.Navigate(new Uri("/StopPage.xaml", UriKind.Relative));

            StopView.SelectedItem = null;
        }
    }
}