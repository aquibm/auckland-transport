﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AT.ViewModels;
using System.Device.Location;
using Microsoft.Phone.Maps.Controls;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;

namespace AT
{
    public partial class StopPage : PhoneApplicationPage
    {

        public StopModel Stop;
        public ObservableCollection<BusModel> Busses;

        public StopPage()
        {
            InitializeComponent();

            // Get Stop from properties.
            Stop = PhoneApplicationService.Current.State["Stop"] as StopModel;

            // Set page title.
            StopTitle.Text = Stop.StopCode + " - " +  Stop.StopName;

            Busses = new ObservableCollection<BusModel>();

            LoadBusses();
            MakeMap();
        }

        public void MakeMap()
        {
            GeoCoordinate location = new GeoCoordinate();
            location.Latitude = Stop.StopLat;
            location.Longitude = Stop.StopLon;

            StopMap.SetView(location, 17);
        }

        public void LoadBusses()
        {
            WebClient wc = new WebClient();
            wc.DownloadStringCompleted += BussesDownloadComplete;
            wc.DownloadStringAsync(new Uri("http://api.maxx.co.nz/RealTime/v2/Departures/Stop/" + Stop.StopCode, UriKind.Absolute));
        }

        private void BussesDownloadComplete(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(e.Result));
                StopDataModel StopData = new StopDataModel();
                DataContractJsonSerializer serialize = new DataContractJsonSerializer(typeof(StopDataModel));
                StopData = (StopDataModel)serialize.ReadObject(ms);

                if (StopData.Error == null)
                {
                    foreach (Movement m in StopData.Movements)
                    {
                        m.ActualArrivalTime = m.ActualArrivalTime.ToLocalTime();
                        string Expected = " ";

                        if (m.ExpectedArrivalTime != null)
                        {
                            DateTime ExpectedArrival = ((DateTime)m.ExpectedArrivalTime).ToLocalTime();
                            TimeSpan t = ExpectedArrival.Subtract(DateTime.Now.ToLocalTime());
                            Expected = "" + t.ToString("mm");
                        }

                        Busses.Add(new BusModel
                            {
                                Route = m.Route,
                                Destination = m.DestinationDisplay,
                                ScheduledTime = m.ActualArrivalTime.ToString("HH:mm"),
                                ExpectedTime = Expected
                            }
                        );
                    }

                    BusView.ItemsSource = Busses;
                }
                else
                {
                    MessageBox.Show("Something went wrong while parsing bus data.");
                }
            }
            else
            {
                MessageBox.Show("Something went wrong while downloading bus data.");
            }
        }
    }
}